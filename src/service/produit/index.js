import API from "../../config/axios";
import {PRODUIT_ENDPOINT, PRODUIT_NO_JSON_ENDPOINT} from "../endpoints";

export const getProduits = () => API.get(PRODUIT_ENDPOINT)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const postProduit = (produit) => API.post(PRODUIT_ENDPOINT, produit)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const putProduit = (produit) => API.put(PRODUIT_ENDPOINT + "/" + produit.id, produit)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const deleteProduit = (id) => API.delete(PRODUIT_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const getProduit = (id) => API.get(PRODUIT_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});