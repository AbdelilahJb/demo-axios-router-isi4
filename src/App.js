import React, {Suspense} from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from "./pages/Home";
import {routes} from './routes';
import {isEmpty} from "lodash";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Switch>
                    <Route path={"/"} component={Home} exact/>
                    <Suspense fallback={<div>Loading...</div>}>
                        {
                            !isEmpty(routes) && routes.map(route => {
                                return <Route path={route.path} component={route.component} exact/>;
                            })
                        }
                    </Suspense>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;