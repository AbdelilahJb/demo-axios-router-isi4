import React, {lazy} from 'react';

const Produits = lazy(() => import("../pages/produits"));
const ProduitForm = lazy(() => import("../pages/produits/form"));
const Produit = lazy(() => import("../pages/produits/show"));

export const routes = [
    {path: "/products", component: Produits},
    {path: "/products/add", component: ProduitForm},
    {path: "/products/:id", component: Produit},
];