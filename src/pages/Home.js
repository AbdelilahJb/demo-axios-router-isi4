import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Home extends Component {
    render() {
        return <div className={"container mt-5"}>
            <Link to={"/products"}>
                <button className={"btn btn-info"}>
                    La liste des produits
                </button>
            </Link>
        </div>
    }
}

export default Home;