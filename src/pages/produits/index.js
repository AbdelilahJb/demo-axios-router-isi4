import React, {Component} from "react";
import {Card, CardHeader, CardBody, Table, Col} from "reactstrap";
import {getProduits, deleteProduit} from "../../service/produit";
import {Link} from "react-router-dom";
import Row from "reactstrap/es/Row";

class Produits extends Component {

    constructor(props) {
        super(props);
        this.state = {
            produits: []
        }
    }

    async componentDidMount() {
        const produits = await getProduits();
        this.setState({produits})
    }

    render() {
        const {produits} = this.state;
        return <div className={"container mt-5"}>
            <Card>
                <CardHeader>
                    <Row>
                        <Col md={6}>La listes des produits</Col>
                        <Col md={6} className={"d-flex justify-content-end"}>
                            <Link to={"products/add"}>
                                <button className={"btn btn-success"}>
                                    Ajouter un produit
                                </button>
                            </Link>
                        </Col>
                    </Row>

                </CardHeader>
                <CardBody>
                    <Table>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            produits.map(produit => {
                                return <tr key={produit.id}>
                                    <td>{produit.id}</td>
                                    <td>{produit.name}</td>
                                    <td>{produit.description}</td>
                                    <td>
                                        <span
                                            onClick={() => this.showDetails(produit.id)}
                                            className={"btn btn-info mr-2"}>
                                            Details
                                        </span>
                                        <span
                                            onClick={() => this.deleteProduit(produit.id)}
                                            className={"btn btn-danger"}>
                                            Supprimer
                                        </span>
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                    </Table>
                    <Link to={'/'}>
                        <button className={"btn btn-danger mt-3"}>
                            Back Home
                        </button>
                    </Link>
                </CardBody>
            </Card>
        </div>
    }

    deleteProduit = async (id) => {
        await deleteProduit(id);
        await getProduits();
    };

    showDetails = (id) => {
        this.props.history.push("/products/" + id);
    }
}

export default Produits