import React, {Component} from 'react';
import {getProduit} from "../../service/produit";
import {Card, CardBody, CardHeader, Col} from "reactstrap";
import {Link} from "react-router-dom";

class Produit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            produit : {}
        }
    }

    async componentDidMount() {
        const produit = await getProduit(this.props.match.params.id);
        this.setState({produit})
    }

    render() {
        const {produit} = this.state;
        return <div className={"container mt-5"}>
            <Card>
                <CardHeader>{produit && produit.name}</CardHeader>
                <CardBody>
                    {produit && produit.description}
                    <Col>
                    <Link to={'/products'}>
                        <button className={"btn btn-danger mt-3"}>
                            Liste des produits
                        </button>
                    </Link>
                    </Col>
                </CardBody>
            </Card>
        </div>
    }
}

export default Produit;