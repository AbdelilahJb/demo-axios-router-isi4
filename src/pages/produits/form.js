import React, {Component} from "react";
import {Card, CardHeader, CardBody, Input, FormGroup, Label, Col, Row} from "reactstrap";
import {postProduit} from "../../service/produit";

class ProduitForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: "",
            description: ""
        };
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value});
    };

    onDescriptionChange = (e) => {
        this.setState({description: e.target.value});
    };

    render() {
        const {name, description} = this.state;
        return <div className={"container mt-5"}>
            <Card>
                <CardHeader>Ajouter un produit :</CardHeader>
                <CardBody>
                    <form>
                        <FormGroup>
                            <Label>Nom : </Label>
                            <Input
                                name={"name"}
                                type={"text"}
                                placeholder={"Nom du produit"}
                                value={name}
                                onChange={this.onNameChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Description : </Label>
                            <Input
                                name={"description"}
                                type={"textarea"}
                                placeholder={"Description du produit"}
                                value={description}
                                onChange={this.onDescriptionChange}
                            />
                        </FormGroup>
                        <Row>
                            <Col className={"offset-10"}>
                                <span className={"btn btn-outline-warning"}
                                      onClick={this.ajouterProduit}
                                >
                                    Ajouter
                                </span>
                            </Col>
                        </Row>
                    </form>
                </CardBody>
            </Card>

        </div>;
    }

    ajouterProduit = async () => {
        const {name, description} = this.state;
        const produit = {name, description};
        await postProduit(produit);
            this.props.history.push("/products");
    }
}

export default ProduitForm;